package tile;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.imageio.ImageIO;

import main.GamePanel;
import main.UtilityTool;

public class TileManager {

	GamePanel gp;
	public Tile[] tile;
	public int mapTileNum[][];
	
	public TileManager(GamePanel gp) {
		
		this.gp = gp;
		
		tile = new Tile[500];
		mapTileNum = new int[gp.maxScreenCol][gp.maxScreenRow];
		
		mapTileNum = new int[gp.maxScreenCol][gp.maxScreenRow];
		
		getTileImage();
		loadMap("/maps/Poli");
	}
	
	public void getTileImage() {
		setup(0, "vao", true);
		setup(1, "vao", true);
		setup(2, "vao", true);
		setup(3, "vao", true);
		setup(4, "vao", true);
		setup(5, "vao", true);
		setup(6, "vao", true);
		setup(7, "vao", true);
		setup(8, "vao", true);
		setup(9, "vao", true);
		setup(10, "vao", true);
		setup(11, "vao", true);
		setup(12, "vao", true);
		setup(13, "vao", true);
		setup(14, "vao", true);
		setup(15, "vao", true);
		setup(16, "vao", true);
		setup(17, "vao", true);
		setup(18, "vao", true);
		setup(19, "vao", true);
		setup(20, "vao", true);
		setup(21, "vao", true);
		setup(22, "vao", true);
		setup(23, "vao", true);
		setup(24, "vao", true);
		setup(25, "vao", true);
		setup(26, "vao", true);
		setup(27, "vao", true);
		setup(28, "vao", true);
		setup(29, "vao", true);
		setup(30, "vao", true);
		setup(31, "vao", true);
		setup(32, "vao", true);
		setup(33, "vao", true);
		setup(34, "vao", true);
		setup(35, "vao", true);
		setup(36, "vao", true);
		setup(37, "vao", true);
		setup(38, "vao", true);
		setup(39, "vao", true);
		setup(40, "vao", true);
		setup(41, "vao", true);
		setup(42, "vao", true);
		setup(43, "vao", true);
		setup(44, "vao", true);
		setup(45, "vao", true);
		setup(46, "vao", true);
		setup(47, "vao", true);
		setup(48, "vao", true);
		setup(49, "vao", true);
		setup(50, "vao", true);
		setup(51, "vao", true);
		setup(52, "vao", true);
		setup(53, "vao", true);
		setup(54, "vao", true);
		setup(55, "vao", true);
		setup(56, "vao", true);
		setup(57, "vao", true);
		setup(58, "vao", true);
		setup(59, "vao", true);
		setup(60, "vao", true);
		setup(61, "vao", true);
		setup(62, "vao", true);
		setup(63, "vao", true);
		setup(64, "vao", true);
		setup(65, "vao", true);
		setup(66, "vao", true);
		setup(67, "vao", true);
		setup(68, "vao", true);
		setup(69, "vao", true);
		setup(70, "vao", true);
		setup(71, "vao", true);
		setup(72, "vao", true);
		setup(73, "vao", true);
		setup(74, "vao", true);
		setup(75, "vao", true);
		setup(76, "vao", true);
		setup(77, "vao", true);
		setup(78, "vao", true);
		setup(79, "vao", true);
		setup(80, "vao", true);
		setup(81, "vao", true);
		setup(82, "vao", true);
		setup(83, "vao", true);
		setup(84, "vao", true);
		setup(85, "vao", true);
		setup(86, "vao", true);
		setup(87, "vao", true);
		setup(88, "vao", true);
		setup(89, "vao", true);
		setup(90, "vao", true);
		setup(91, "vao", true);
		setup(92, "vao", true);
		setup(93, "vao", true);
		setup(94, "vao", true);
		setup(95, "vao", true);
		setup(96, "vao", true);
		setup(97, "vao", true);
		setup(98, "vao", true);
		setup(99, "vao", true);
		setup(100, "arvore1", true);
		setup(101, "arvore2", true);
		setup(102, "arvore3", true);
		setup(103, "arvore4", true);
		setup(104, "arvore5", true);
		setup(105, "arvore6", true);
		setup(106, "arvore7", true);
		setup(107, "arvore8", true);
		setup(108, "arvore9", true);
		setup(109, "arvore10", true);
		setup(110, "arvore11", true);
		setup(111, "arvore12", true);
		setup(112, "arvore13", true);
		setup(113, "arvore14", true);
		setup(114, "arvore15", true);
		setup(115, "arvore16", true);
		setup(116, "arvore17", true);
		setup(117, "arvore18", true);
		setup(118, "arvore19", true);
		setup(119, "arvore20", true);
		setup(120, "arvore21", true);
		setup(121, "floor", false);
		setup(122, "grass", false);
		setup(123, "gym1", true);
		setup(124, "gym2", true);
		setup(125, "gym3", true);
		setup(126, "gym4", true);
		setup(127, "gym5", true);
		setup(128, "gym6", true);
		setup(129, "gym7", true);
		setup(130, "gym8", true);
		setup(131, "gym9", true);
		setup(132, "gym10", true);        
		setup(133, "gym11", true);        
		setup(134, "gym12", true);        
		setup(135, "gym13", true);        
		setup(136, "gym14", true);        
		setup(137, "gym15", true);        
		setup(138, "gym16", true);        
		setup(139, "gym17", true);        
		setup(140, "gym18", true);        
		setup(141, "gym19", true);        
		setup(142, "gym20", true);        
		setup(143, "gym21", true);        
		setup(144, "gym22", true);        
		setup(145, "gym23", true);        
		setup(146, "gym24", true);        
		setup(147, "gym25", true);        
		setup(148, "gym26", true);        
		setup(149, "gym27", true);        
		setup(150, "gym28", true);        
		setup(151, "gym29", true);        
		setup(152, "gym30", true);        
		setup(153, "gym31", true);        
		setup(154, "gym32", true);        
		setup(155, "gym33", true);        
		setup(156, "gym34", true);        
		setup(157, "gym35", true);        
		setup(158, "gym36", true);        
		setup(159, "gym37", true);        
		setup(160, "gym38", true);        
		setup(161, "gym39", true);        
		setup(162, "gym40", true);        
		setup(163, "gym41", true);        
		setup(164, "gym42", true);        
		setup(165, "gym43", true);        
		setup(166, "gym44", true);        
		setup(167, "gym45", true);        
		setup(168, "gym46", true);        
		setup(169, "gym47", true);        
		setup(170, "gym48", true);        
		setup(171, "gym49", true);        
		setup(172, "gym50", true);        
		setup(173, "gym51", true);        
		setup(174, "gym52", true);        
		setup(175, "gym53", true);        
		setup(176, "gym54", true);        
		setup(177, "gym55", true);        
		setup(178, "gym56", true);        
		setup(179, "gym57", true);        
		setup(180, "gym58", true);        
		setup(181, "gym59", true);        
		setup(182, "gym60", true);        
		setup(183, "gym61", true);        
		setup(184, "gym62", true);        
		setup(185, "gym63", true);
		setup(186, "gym64", true);        
		setup(187, "gym65", true);        
		setup(188, "gym66", true);        
		setup(189, "gym67", true);        
		setup(190, "gym68", true);        
		setup(191, "gym69", true);        
		setup(192, "gym70", true);        
		setup(193, "gym71", true);        
		setup(194, "gym72", true);
		setup(195, "island-down", false);
		setup(196, "island-downleft", false);
		setup(197, "island-downleft2", false);
		setup(198, "island-downright", false);
		setup(199, "island-downright2", false);
		setup(200, "island-left", false);
		setup(201, "island-right", false);
		setup(202, "island-up", false);
		setup(203, "market1", true);
		setup(204, "market2", true);      
		setup(205, "market3", true);      
		setup(206, "market4", true);      
		setup(207, "market5", true);      
		setup(208, "market6", true);      
		setup(209, "market7", true);      
		setup(210, "market8", true);      
		setup(211, "market9", true);      
		setup(212, "market10", true);     
		setup(213, "market11", true);     
		setup(214, "market12", true);     
		setup(215, "market13", true);     
		setup(216, "market14", true);     
		setup(217, "market15", true);     
		setup(218, "market16", true);     
		setup(219, "market17", true);     
		setup(220, "market18", true);     
		setup(221, "market19", true);     
		setup(222, "market20", true); 
		setup(223, "path-downleft", false);
		setup(224, "path-downleft2", false);
		setup(225, "path-downright", false);
		setup(226, "path-downright2", false);
		setup(227, "path-left", false);
		setup(228, "path-right", false);
		setup(229, "path-up", false);
		setup(230, "path-upleft", false);
		setup(231, "path-upleft2", false);
		setup(232, "path-upright", false);
		setup(233, "path-upright2", false);
		setup(234, "pokecenter1", true);
		setup(235, "pokecenter2", true);  
		setup(236, "pokecenter3", true);  
		setup(237, "pokecenter4", true);  
		setup(238, "pokecenter5", true);  
		setup(239, "pokecenter6", true);  
		setup(240, "pokecenter7", true);  
		setup(241, "pokecenter8", true);  
		setup(242, "pokecenter9", true);  
		setup(243, "pokecenter10", true); 
		setup(244, "pokecenter11", true); 
		setup(245, "pokecenter12", true); 
		setup(246, "pokecenter13", true); 
		setup(247, "pokecenter14", true); 
		setup(248, "pokecenter15", true); 
		setup(249, "pokecenter16", true); 
		setup(250, "pokecenter17", true); 
		setup(251, "pokecenter18", true); 
		setup(252, "pokecenter19", true); 
		setup(253, "pokecenter20", true); 
		setup(254, "pokecenter21", true); 
		setup(255, "pokecenter22", true); 
		setup(256, "pokecenter23", true); 
		setup(257, "pokecenter24", true); 
		setup(258, "pokecenter25", true); 
		setup(259, "pokecenter26", true); 
		setup(260, "pokecenter27", true); 
		setup(261, "pokecenter28", true); 
		setup(262, "pokecenter29", true); 
		setup(263, "pokecenter30", true); 
		setup(264, "pokecenter31", true); 
		setup(265, "pokecenter32", true); 
		setup(266, "pokecenter33", true); 
		setup(267, "pokecenter34", true); 
		setup(268, "pokecenter35", true); 
		setup(269, "pokecenter36", true);
		setup(270, "ponte-down", false);
		setup(271, "ponte-downleft", false);
		setup(272, "ponte-downright", false);
		setup(273, "ponte-up", false);
		setup(274, "ponte-upleft", false);
		setup(275, "ponte-upright", false);
		setup(276, "street-down", false);
		setup(277, "street-downleft", false);
		setup(278, "street-downleft2", false);
		setup(279, "street-downright", false);
		setup(280, "street-downright2", false);
		setup(281, "street-left", false);
		setup(282, "street-right", false);
		setup(283, "street-up", false);
		setup(284, "street-upleft", false);
		setup(285, "street-upleft2", false);
		setup(286, "street-upright", false);
		setup(287, "street-upright2", false);
		setup(288, "vao-down", false);
		setup(289, "vao-downleft", false);
		setup(290, "vao-downleft2", false);
		setup(291, "vao-downright2", false);
		setup(292, "vao-left", false);
		setup(293, "vao-right", false);
		setup(294, "vao-up", false);
		setup(295, "vao-upleft", false);
		setup(296, "vao-upleft2", false);
		setup(297, "vao-upright", false);
		setup(298, "vao-upright2", false);
		setup(299, "water", false);
		setup(300, "wild", false);
		setup(301, "wild-down", false);
		setup(302, "wild-downleft", false);
		setup(303, "wild-downright", false);
		setup(304, "wild-right", false);
		setup(305, "wild-up", false);
		setup(306, "wild-upleft", false);
		setup(307, "wild-upright", false);
		setup(308, "street", false);
		setup(309, "wild-left", false);
		setup(310, "path", false);
		setup(311, "path-down", false);
		setup(312, "vao", false);
		setup(313, "vao-downright", false);
			
			//JIMPA ESCREVA setup(i, "name", collision);
		
		
	}
	
	public void setup(int index, String imageName, boolean collision) {
	
		UtilityTool uTool = new UtilityTool();
		
		try {
			tile[index] = new Tile();
			tile[index].image = ImageIO.read(getClass().getResourceAsStream("/tiles/" + imageName + ".png"));
			tile[index].image = uTool.scaleImage(tile[index].image, gp.tileSize, gp.tileSize);
			tile[index].collision = collision;
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadMap(String mapPath) {
		
		try {
			InputStream is = getClass().getResourceAsStream(mapPath);
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			int col =0;
			int row =0;

			while (col < gp.maxWorldCol && row < gp.maxWorldRow) {
				
				String line = br.readLine();
				
				while(col < gp.maxWorldCol) {
					
					String numbers[] = line.split(" ");
					
					int num = Integer.parseInt(numbers[col]);
					
					mapTileNum[col][row] = num;
					col++;
				}
				if(col == gp.maxWorldCol) {
					col = 0;
					row ++;
				}
			}
			br.close();
			
		}catch(Exception e){
			
		}
	}
	public void draw(Graphics2D g2) {
		
		//g2.drawImage(tile[1].image, 0, 0, tile[1].image.getWidth(), tile[1].image.getHeight(), null);
	
		int worldCol = 0;
		int worldRow = 0;

		
		while(worldCol < gp.maxWorldCol && worldRow < gp.maxWorldRow) {
			
			int tileNum = mapTileNum[worldCol][worldRow];
			
			int worldX = worldCol * gp.tileSize;
			int worldY = worldRow * gp.tileSize;
			int screenX = worldX - gp.player.worldX + gp.player.screenX;
			int screenY = worldY - gp.player.worldY + gp.player.screenY;
			
			if( worldX + gp.tileSize > gp.player.worldX - gp.player.screenX &&
				worldX - gp.tileSize < gp.player.worldX + gp.player.screenX &&
				worldY + gp.tileSize > gp.player.worldY - gp.player.screenY &&
				worldY - gp.tileSize < gp.player.worldY + gp.player.screenY) {
				
				g2.drawImage(tile[tileNum].image, screenX, screenY, gp.tileSize, gp.tileSize, null);
			}
			
			worldCol++;

			if(worldCol == gp.maxScreenCol) {
				worldCol = 0;
				worldRow++;

			}
			
		}
	}
	
	BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight) throws IOException {
	    BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_RGB);
	    Graphics2D graphics2D = resizedImage.createGraphics();
	    graphics2D.drawImage(originalImage, 0, 0, null);
	    graphics2D.dispose();
	    return resizedImage;
	}

}
