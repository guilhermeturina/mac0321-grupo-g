package entity;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.GamePanel;
import main.KeyHandler;
import main.UtilityTool;

public class Polimons extends Entity {
	
	public int tipo;
	public int nivel;
	public int [] movimentos;
	GamePanel gp;
	KeyHandler keyH;
	

	
	public Polimons(int tipo, int nivel, int[] movimentos, GamePanel gp, KeyHandler keyH) {
		super();
		this.tipo = tipo;
		this.nivel = nivel;
		this.movimentos = movimentos;
		this.gp = gp;
		this.keyH = keyH;
	}

	public void getPolimonsImage() {
		//jimpa definir qual polimon é usano tipo 
	
		def = setup("Poli/Walking/Up1");
	
	}
	
	public BufferedImage setup(String imageName) {
		
		UtilityTool uTool = new UtilityTool();
		BufferedImage image = null;
		
		try {
			image = ImageIO.read(getClass().getResourceAsStream("/player/"+imageName+".png"));
			image = uTool.scaleImage(image, gp.tileSize, gp.tileSize);
		}catch(IOException e) {
			e.printStackTrace();
		}
		return image;
	}
	
	
	
	

}
