package entity;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;                                                                                
//Superclasse para todas as entidades
public class Entity {

	public int worldX, worldY;
	public int speed;
	
	public BufferedImage up1, up2, down1, down2, left1, left2, right1, right2;
	public BufferedImage def;
	public BufferedImage upBike1, upBike2, upBike3, downBike1, downBike2, downBike3, rightBike1, rightBike2, rightBike3, leftBike1, leftBike2, leftBike3;
	public String direction;
	
	public int spriteCounter = 0;
	public int spriteNum = 1; 

	public Rectangle solidArea;
	public int solidAreaDefaultX, solidAreaDefaultY;
	public boolean collisionOn = false;
}
