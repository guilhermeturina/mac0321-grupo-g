package entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.GamePanel;
import main.KeyHandler;
import main.UtilityTool;

public class Player extends Entity{

	GamePanel gp;
	KeyHandler keyH;
	Polimons [] bag;
	public final int screenX;//onde desenhar o player na tela
	public final int screenY;
	public int hasPoliballs = 0; 
	boolean hasBike = false, ridingBike = false; //controlam a bike
	
	public Player(GamePanel gp, KeyHandler keyH) {
		
		this.gp = gp;
		this.keyH = keyH;
		
		bag = new Polimons [2];
		
		screenX = gp.screenWidth/2 - (gp.tileSize/2);
		screenY = gp.screenHeight/2 - (gp.tileSize/2);

	    solidArea = new Rectangle(0,0,48,48); //x,y,altura,largura
	    solidArea.x = 8;
	    solidArea.y = 16;
	    solidAreaDefaultX = solidArea.x;
	    solidAreaDefaultY = solidArea.y;
	    solidArea.width = 32;
	    solidArea.height = 32;
		
		setDefaultValues();
		getPlayerImage();
	}
	
	public void setDefaultValues() {
		 
		worldX = gp.tileSize * 20;//coordenadas de spawn a definir
		worldY = gp.tileSize * 140;
		speed = 4;
		direction = "down";
	}
	
	public void getPlayerImage() {
		//jimpa pega as imagens
			
			up1 = setup("Poli/Walking/Up1");
			up2 = setup("Poli/Walking/Up2");
			down1 = setup("Poli/Walking/Down1");
			down2 = setup("Poli/Walking/Down2");
			left1 = setup("Poli/Walking/Left1");
			left2 = setup("Poli/Walking/Left2");
			right1 = setup("Poli/Walking/Right1");
			right2 = setup("Poli/Walking/Right2");
			
			upBike1 = setup("Poli/Bike/Riding/Up1");
			upBike2 = setup("Poli/Bike/Riding/Up2");
			upBike3 = setup("Poli/Bike/Riding/Up3");
			downBike1 = setup("Poli/Bike/Riding/Down1");
			downBike2 = setup("Poli/Bike/Riding/Down2");
			downBike3 = setup("Poli/Bike/Riding/Down3");
			leftBike1 = setup("Poli/Bike/Riding/Left1");
			leftBike2 = setup("Poli/Bike/Riding/Left2");
			leftBike3 = setup("Poli/Bike/Riding/Left3");
			rightBike1 = setup("Poli/Bike/Riding/Right1");
			rightBike2 = setup("Poli/Bike/Riding/Right2");
			rightBike3 = setup("Poli/Bike/Riding/Right3");
		
		
		
	}
	
	public BufferedImage setup(String imageName) {
		
		UtilityTool uTool = new UtilityTool();
		BufferedImage image = null;
		
		try {
			image = ImageIO.read(getClass().getResourceAsStream("/player/"+imageName+".png"));
			image = uTool.scaleImage(image, gp.tileSize, gp.tileSize);
		}catch(IOException e) {
			e.printStackTrace();
		}
		return image;
	}
	
	public void update() {System.out.println(ridingBike);
		
		if(keyH.upPressed == true || keyH.downPressed == true || keyH.leftPressed == true || keyH.rightPressed == true) {
			
			if(keyH.upPressed == true) {
				direction = "up";
				
			}
			
			else if (keyH.downPressed == true) {
				direction = "down";
				
			}
			
			else if(keyH.leftPressed == true) {
				direction = "left";
				
			}
			
			else if(keyH.rightPressed == true) {
				direction = "right";
				
			}
			
			//ve se bateu
			collisionOn = false;
			gp.cChecker.checkTile(this);
			
			//ve se bate em objeto
			int objectIndex = gp.cChecker.checkObject(this, true);
			pickUpObject(objectIndex);
			
			
			//ve evento 
			
			gp.eHandler.checkEvente();
			
		
			
			//se nao bater
			if(collisionOn == false) {
				switch(direction) {
				case"up":
					worldY -= speed;
					break;
				case"down":
					worldY += speed;
					break;
				case"left":
					worldX -= speed;
					break;
				case"right":
					worldX += speed;
					break; 
				}
			}
			
			spriteCounter++;
			if(spriteCounter > 12 && ridingBike == false){
				if(spriteNum == 1) {
					spriteNum = 2;
				}
				
				else if (spriteNum == 2) {
					spriteNum = 1;
				}
				spriteCounter = 0;
			}
			
			if(spriteCounter > 12 && ridingBike == true){//Como a bike tem 3 sprites vamos fazer outro if
				if(spriteNum == 1) {
					spriteNum = 2;
				}
				
				else if (spriteNum == 2) {
					spriteNum = 3;
				}
				
				else if(spriteNum == 3) {
					spriteNum = 1;
				}
				spriteCounter = 0;
			}
		}
		
		if(keyH.bikePressed == true) {
			if(hasBike == true) {
				if(ridingBike == false) {
					ridingBike = true;
					speed += 4;
				}
				
				else {
					ridingBike = false;
					speed -= 4;
					spriteNum = 1; // Pra nao ficar invisivel quando desce da bike
				}
			}
		}
		
	}
	
	public void pickUpObject(int i) {
		
		if(i != 999) {
			
			String objectName = gp.obj[i].name;
			
			switch(objectName) {
			case "Poliball":
				gp.playSE(1);
				hasPoliballs++;
				gp.obj[i] = null;
				gp.ui.showMessage("Você pegou uma pokebola");
				break;
				
			case "Bike":
				hasBike = true;
				gp.obj[i] = null;
				break;
			}
		}
		
	}
	
	public void draw(Graphics2D g2) {
		
		BufferedImage image = null;
		
		switch(direction) {
		case "up":
			if(ridingBike == false) {
				if(spriteNum == 1) {
					image = up1;
				}
				
				if(spriteNum == 2) {
					image = up2;
				}
			}
			
			else {
				if(spriteNum == 1) {
					image = upBike1;
				}
				
				if(spriteNum == 2) {
					image = upBike2;
				}
				
				if(spriteNum == 3) {
					image = upBike3;
				}
			}
			break;
			
		case "down":
			if(ridingBike == false) {
				if(spriteNum == 1) {
					image = down1;
				}
				
				if(spriteNum == 2) {
					image = down2;
				}
			}
			
			else {
				if(spriteNum == 1) {
					image = downBike1;
				}
				
				if(spriteNum == 2) {
					image = downBike2;
				}
				
				if(spriteNum == 3) {
					image = downBike3;
				}
			}
			break;
			
		case "left":
			if(ridingBike == false) {
				if(spriteNum == 1) {
					image = left1;
				}
				
				if(spriteNum == 2) {
					image = left2;
				}
			}
			
			else {
				if(spriteNum == 1) {
					image = leftBike1;
				}
				
				if(spriteNum == 2) {
					image = leftBike2;
				}
				
				if(spriteNum == 3) {
					image = leftBike3;
				}
			}
			break;
			
		case "right":
			if(ridingBike == false) {
				if(spriteNum == 1) {
					image = right1;
				}
				
				if(spriteNum == 2) {
					image = right2;
				}
			}
			
			else {
				if(spriteNum == 1) {
					image = rightBike1;
				}
				
				if(spriteNum == 2) {
					image = rightBike2;
				}
				
				if(spriteNum == 3) {
					image = rightBike3;
				}
			}
			break;
		}
		g2.drawImage(image, screenX, screenY, null);
	}
}
