package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

import entity.Player;
import object.SuperObject;
import tile.TileManager;

public class GamePanel extends JPanel implements Runnable {

	//Configurações de tela
	final int originalTileSize = 16;
	final int scale = 5; //escala da imagem
	
	public final int tileSize = originalTileSize * scale; //48x48 tile
	public final int maxScreenCol = 80;
	public final int maxScreenRow = 150;
	
	//A tela segue a resolução do GameBoy (240x160 pixels)
	public final int screenWidth = scale * 240;
	public final int screenHeight = scale * 160;  
	
	//Configurações do mundo
	public final int maxWorldCol = 80;
	public final int maxWorldRow = 150;
	
	//FPS
	int FPS = 60;
	
	//Sistema
	TileManager tileM = new TileManager(this);
	public KeyHandler keyH = new KeyHandler(this);
	Sound se = new Sound();
	Sound music = new Sound();
	public CollisionChecker cChecker = new CollisionChecker(this); 
	public AssetSetter aSetter = new AssetSetter(this);
	public UI ui = new UI(this);
	public EventHandler eHandler = new EventHandler(this);
	Thread gameThread;
	
	//Entidadde e Objetos
	public Player player = new Player(this, keyH);
	public SuperObject obj [] = new SuperObject[3];
	
	//game state
	public int gameState;
	public final int play = 1;
	public final int stop = 2;
	
	
	public GamePanel() {
		this.setPreferredSize(new Dimension(screenWidth, screenHeight ));
		this.setBackground(Color.black);
		this.setDoubleBuffered(true); //Jogo roda mais clean
		this.addKeyListener(keyH);
		this.setFocusable(true);
	}

	
	public void setupGame() {
		
		aSetter.setObject();
		playMusic(0);
		gameState = play;
	}
	
	public void startGameThread() {
		
		gameThread = new Thread(this);
		gameThread.start();
	}

	@Override
	public void run() {
		
		double drawInterval = 1000000000/FPS;
		double delta = 0;
		long lastTime =System.nanoTime();
		long currentTime;
		long timer = 0;
		int drawCount = 0;
		
		while(gameThread != null) {
			
			currentTime = System.nanoTime();
			
			delta += (currentTime - lastTime) / drawInterval;
			timer += (currentTime - lastTime);
			lastTime = currentTime;
			
			if(delta >= 1) {
				
				// 1 Update: atualizar a posição do personagem
				update();
				
				//2 Draw: fazer o boneco andar na tela com a informação atualizada
				repaint();
				
				delta--;
				drawCount += 1;
				
			}
			
			if(timer >= 1000000000) {
				System.out.println("FPS:"+drawCount);
				drawCount = 0;
				timer = 0;
			}
	
		}
		
	}
	
	public void update() {
		if(gameState == play) {
			
			player.update();
		}
		
	}
	
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g;
		
		//DEBUG
		long drawStart = 0;
		if(keyH.checkDrawTime == true) {
			drawStart = System.nanoTime();
		}
		
		//Tile
		tileM.draw(g2);
		
		//Object
		for(int i = 0; i < obj.length; i++) {
			if(obj[i] != null) {
				obj[i].draw(g2, this);
			}
		}
		
		//Player
		player.draw(g2);
		
		//UI
		ui.draw(g2);
		
		//DEBUG
		if(keyH.checkDrawTime == true) {
			long drawEnd = System.nanoTime();
			long passed = drawEnd - drawStart;
			g2.setColor(Color.white);
			g2.drawString("Draw time " + passed, 10, 400);
			System.out.println("Draw Time: "+passed);
		}
		
		g2.dispose();
	}
	
	public void playMusic(int i) {
		
		music.setFile(i);
		music.play();
		music.loop();
	}
	
	public void stopMusic() {
		music.stop();
	}
	
	public void playSE(int i) {
		se.setFile(i);
		se.play();
	}
}
