package main;

import java.awt.Rectangle;

public class EventHandler {
	
	GamePanel gp;
	Rectangle eventReact;
	int eventDefaultRecX, eventDefaultRecY;
	 
	public EventHandler (GamePanel gp) {
		this.gp = gp;
		
		eventReact = new Rectangle();
		eventReact.x = 23; 
		eventReact.y = 23;
		eventReact.height = 2;
		eventReact.width = 2;
		eventDefaultRecY = eventReact.x;
		eventDefaultRecX = eventReact.y;
	}
	
	public void checkEvente() {
		//coloca o hit no lugar do evento e faz a classe do evento la embaixo 
		
	}

	public  boolean hit (int eventCol, int eventRow, String reqDirection) {
		
		boolean hit = false;
		
		
		gp.player.solidArea.x = gp.player.worldX + gp.player.solidArea.x;
		gp.player.solidArea.y = gp.player.worldY + gp.player.solidArea.y;
		eventReact.x = eventCol*gp.tileSize + gp.player.solidArea.x;
		eventReact.y = eventRow*gp.tileSize + gp.player.solidArea.y;
		
		if(gp.player.solidArea.intersects(eventReact)) {
			if(gp.player.direction.contentEquals(reqDirection)||reqDirection.contentEquals("any")){
			
				hit = true;
			}
		}
		
		gp.player.solidArea.x = gp.player.solidAreaDefaultX;
		gp.player.solidArea.y = gp.player.solidAreaDefaultY;
		eventReact.x = eventDefaultRecX;
		eventReact.y = eventDefaultRecY;
		
		
		return hit;
				
				
				
	}
}
