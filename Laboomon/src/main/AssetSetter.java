package main;

import object.OBJ_Bike;
import object.OBJ_Poliball;

public class AssetSetter {

	GamePanel gp;
	
	public AssetSetter(GamePanel gp) {
		this.gp = gp;
	}
	
	public void setObject() {
		
		gp.obj[0] = new OBJ_Poliball(gp);
		gp.obj[0].worldX = 10 * gp.tileSize;
		gp.obj[0].worldY = 140 * gp.tileSize;
		
		gp.obj[1] = new OBJ_Poliball(gp);
		gp.obj[1].worldX = 40 * gp.tileSize;
		gp.obj[1].worldY = 100 * gp.tileSize;
		
		gp.obj[2] = new OBJ_Bike(gp);
		gp.obj[2].worldX = 30 * gp.tileSize;
		gp.obj[2].worldY = 140 * gp.tileSize;
	}
}